using System;
using System.Text;

public class Vector
{
	// Elements of vector.
	private int[] _numbers;
	
	// Low boundary of vector range.
	private int _startIndex;
	
	// Default constructor.
	public Vector(): this(0, 0)
	{
	}
	
	// Custom constructor.
	public Vector(int length, int startIndex = 0)
	{
		_startIndex = startIndex;
		
		if(length >= 0)
		{
			_numbers = new int[length];
		}
		else
		{
			throw new ArgumentOutOfRangeException("Length cant't be less zero.");
		}
	}
	
	// Custom constructor.
	public Vector(int[] numbers, int startIndex = 0)
	{
		_startIndex = startIndex;
		
		_numbers = new int[numbers.Length];
		
		for(var i = 0; i < numbers.Length; i++)
		{
			_numbers[i] = numbers[i];
		}
	}
	
	// Get start index.
	public int StartIndex
	{
		get
		{
			return _startIndex;
		}
	}
	
	// Get length.
	public int Length
	{
		get
		{
			return _numbers.Length;
		}
	}
	
	// Get/set element of vector by index.
 	public int this[int index]
	{
		get
		{
			if(index >= _startIndex && index < _startIndex + _numbers.Length)
			{
				return _numbers[index - _startIndex];
			}
			else
			{
				throw new IndexOutOfRangeException("Index out of range.");
			}
		}
		
		set
		{
			if(index >= _startIndex && index < _startIndex + _numbers.Length)
			{
				_numbers[index - _startIndex] = value;
			}
			else
			{
				throw new IndexOutOfRangeException("Index out of range.");
			}
		}
	}
	
	// Compare two vectors.
	public override bool Equals(object obj)
	{
		Vector other = obj as Vector;
		
		if(other == null || this.StartIndex != other.StartIndex || this.Length != other.Length)
		{
			return false;
		}
		
		for(var i = StartIndex; i < StartIndex + Length; i++)
		{
			if(this[i] != other[i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	// Compute vector hash code.
	public override int GetHashCode()
	{
		int result = 0;
		
		for(var i = StartIndex; i < StartIndex + Length; i++)
		{
			result = result ^ this[i];
		}
		
		return result;
	}
	
	// Compute vectror presentation.
	public override string ToString()
	{
		StringBuilder result = new StringBuilder();

		for(var i = StartIndex; i < StartIndex + Length; i++)
		{
			result.Append(String.Format("{0}\t", this[i]));
		}
		
		return result.ToString();
	}	
	
	// Add two vectors.
	public static Vector Add(Vector firstTerm, Vector secondTerm)
	{
		if(firstTerm.Length == secondTerm.Length && firstTerm.StartIndex == secondTerm.StartIndex)
		{
			Vector result = new Vector(firstTerm.Length, firstTerm.StartIndex);
			
			for(var i = result.StartIndex; i < result.StartIndex + result.Length; i++)
			{
				result[i] = firstTerm[i] + secondTerm[i];
			}
			
			return result;
		}
		else
		{
			throw new ArgumentOutOfRangeException("Vectors have different ranges.");
		}
	}
	
	// Subtract two vectors.
	public static Vector Subtract(Vector declining, Vector subtrahend)
	{
		if(declining.Length == subtrahend.Length && declining.StartIndex == subtrahend.StartIndex)
		{
			Vector result = new Vector(declining.Length, declining.StartIndex);
			
			for(var i = result.StartIndex; i < result.StartIndex + result.Length; i++)
			{
				result[i] = declining[i] - subtrahend[i];
			}
			
			return result;
		}
		else
		{
			throw new ArgumentOutOfRangeException("Vectors have different ranges.");
		}
	}
	
	// Multiply vector by scalar.
	public static Vector Multiply(Vector multiplier, int scalar)
	{
		Vector result = new Vector(multiplier.Length, multiplier.StartIndex);
		
		for(var i = result.StartIndex; i < result.StartIndex + result.Length; i++)
		{	
			result[i] = multiplier[i] * scalar;
		}
		
		return result;
	}
	
	// Divide vector by scalar.
	public static Vector Divide(Vector devedent, int scalar)
	{
		Vector result = new Vector(devedent.Length, devedent.StartIndex);
		
		for(var i = result.StartIndex; i < result.StartIndex + result.Length; i++)
		{
			result[i] = (int)(devedent[i] / scalar);
		}
		
		return result;
	}
	
	// Compare two vectors using ==. 
	public static bool operator ==(Vector firstVector, Vector secondVector)
	{
		return Equals(firstVector, secondVector);
	}
	
	// Compare two vectors using !=.
	public static bool operator !=(Vector firstVector, Vector secondVector)
	{
		return !Equals(firstVector, secondVector);
	}
	
	// Add two vectors using operator +.
	public static Vector operator +(Vector firstTerm, Vector secondTerm)
	{
		return Add(firstTerm, secondTerm);
	}
	
	// Subtract two vectors using operator -.
	public static Vector operator -(Vector declining, Vector subtrahend)
	{
		return Subtract(declining, subtrahend);
	}
	
	// Mutliply vector by scalar using operator *.
	public static Vector operator *(Vector multiplier, int scalar)
	{
		return Multiply(multiplier, scalar);
	}
	
	// Multiply vector by scalar using operator *.
	public static Vector operator *(int scalar, Vector multiplier)
	{
		return Multiply(multiplier, scalar);
	}
	
	// Divide bector by scalar using operator /.
	public static Vector operator /(Vector devedent, int scalar)
	{
		return Divide(devedent, scalar);
	}
}

public class EntryPoint
{
	// Entry point.
	public static void Main()
	{
		// Initialisation.
		Vector firstVector = new Vector(new int[]{2, 4, 8, 5}, 10);
		Vector secondVector = new Vector(4, 10);
		Vector thirdVector = new Vector(new int[]{2, 4, 8, 5}, 10);
		
		for(var i = secondVector.StartIndex; i < secondVector.StartIndex + secondVector.Length; i++)
		{
			secondVector[i] = 2 * i;
		}
		
		// Print vectors.
		Console.WriteLine("First vector:\n{0}\n", firstVector);
		Console.WriteLine("Second vector:\n{0}\n", secondVector);
		Console.WriteLine("Third vector:\n{0}\n", thirdVector);
		
		// Add and sub vectors.
		Vector addResult = Vector.Add(firstVector, secondVector);
		Vector subResult = firstVector - secondVector;
		
		// Print result of adding and subtracting vectors.
		Console.WriteLine("Result addind first and second vectors:\n{0}\n", addResult);
		Console.WriteLine("Result subtracting first and second vectors:\n{0}\n", subResult);
		
		// Multiply and divide vectors by scalars.
		Vector multResult = (-1) * firstVector;
		Vector divResult = Vector.Divide(secondVector, 2);
		
		// Print result og multiplying and dividing vectors by scalars.
		Console.WriteLine("Result multiplying first vector by -1:\n{0}\n", multResult);
		Console.WriteLine("Result dividing second vector by 2:\n{0}\n", divResult);
		
		// Compare vectors.
		Console.WriteLine("Are first and second vectors equlals? {0}", firstVector == secondVector);
		Console.WriteLine("Are first and third vectors equals? {0}", firstVector == thirdVector);
	}
}